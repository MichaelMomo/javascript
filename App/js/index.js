// Styles
//require("styles/main.css");

// js
//import $ from "jquery";
//import {log, logTitle} from "logger";

// your import
  //import {add, divide, substract, multiplie, PI}  from './Math';
/*coding examples*/

/* Variables: to create a variable we use the keyword "var"
with javascript you don"t necessary need to specified the data type unlike other languages.
*/
console.log("Variables");
var name1 = "michael";
var age = 21;
var hasDriverLicence = true;
var empty =  undefined;

console.log(name1, age, hasDriverLicence, empty);

// let retrieve the dtattypes of any Variables
console.log(name1 + " type= " + typeof name1);
console.log(age + " type= " + typeof age);
console.log(hasDriverLicence + " type= " + typeof hasDriverLicence);
console.log(empty + " type= " + typeof empty);

/*Object: is a collection of group variables*/
console.log("Object");
var person = {
  name: "Dongmo michael",
  age: 21,
  hasDriverLicence: true,
  address: {
    firstLine: "123",
    postCode: "SE1",
    state: "Italy"
  }
};

console.log(person);
console.log(JSON.stringify(person));

// we can have a single property of our Object
console.log(person.name);


/*Arrays: Allows you to store multiple values inside.
for each element in array, there are store in position
 */
console.log("Arrays");
var names = [
  "Dongmo",
  "Momo",
  "Michael",
  "Gardel"
];

console.log(names);
//Access to an element by index
console.log(names[2]);
// have a length
console.log(names.length);

// Loop trouth Arrays
for (var n of names) {
  console.log(n);
}
// Anpther way to loop trouth an Arrays
names.forEach(function(n, index) {
  console.log(index + " - " + n);
});
console.log("Arithmetic operators");
/* Arithmetic operators:
    "+, -, /, *, %"
*/
var addition = 2 + 2;
var substration = 3 - 90;
var division = 10 / 5;
var multiplication = 3 * 30;
var remainder = 90 % 40;

console.log(addition);
console.log(substration);
console.log(division);
console.log(multiplication);
console.log(remainder);

/*
function: is a set of instructions that allows you to perform a task or calculate a values
function can return or not a value. it can return
*/
console.log("function");
function addNumbers(number1, number2) {
  // start function body
  //define logic
  return number1 + number2;
  //end function body
}
// invoke the function
console.log(addNumbers(3, 8));
// some functions of javascript

console.log(names[0].toLowerCase());
console.log(names[1].toUpperCase());
console.log(names[2].endsWith("el"));
console.log(names[3].startsWith("gr"));

// retrieve the keys of an object
console.log(Object.keys(person));
// retrieve the values of an object
console.log(Object.values(person));

/*
Loops
*/

/*
  Break and Continue
  Break: if the condition is verifie we break the loop
  Continue: is certain contition is true we can go back to loop
*/
console.log("Break and Continue");
for (var i = 0; i < 10; i++) {
  console.log(i);
}

var number = 0;
while(number < 5) {
  console.log(number);

if (number ==2){
  break;
}

  number = number + 1;
}

var i = 0;
do {
  i++;

if (i < 5) {
  continue;
}

  console.log(i)
}while(i < 5);

/*
if statement
*/
console.log("if statement");
var person = {
  name: "Michael",
  age: 17
}
if (person.age >= 18) {
  console.log(person.name + " is an adult");
}else if (person.age == 17){
  console.log(person.name + " is about to be an adult");
}else {
  console.log(person.name + " is not an adult");
}

/*
switch statement
*/
console.log("switch statement");
switch(person.age) {
  case 17:
  console.log(person.name + " is about to be an adult");
  break;
  case 18:
  console.log(person.name + " is an adult");
  break;
  default:
  console.log(person.name + " is not an adult");
}

switch(new Date().getDay()) {
  case 1:
  console.log("Oggi è Lunedì");
  break;
  case 2:
  console.log("Oggi è Martedì");
  break;
  case 3:
  console.log("Oggi è Mercoledì");
  break;
  case 4:
  console.log("Oggi è Giovedì");
  break;
  case 5:
  console.log("Oggi è Venerdì");
  break;
  case 6:
  console.log("Oggi è Sabato");
  break;
  case 7:
  console.log("Oggi è Domedica");
  break;
  default:
  console.log("Giorno non definito");
}

/*
Comparison and logical operators
*/
console.log("Comparison operators");
console.log(10 == 10);
console.log(10 < 10);
console.log(10 <= 10);
console.log(10 > 10);
console.log(10 >= 10);
console.log(10 != 10);
console.log(10 != 11);
console.log("Logical operators");
console.log(10 == 10 && 20 == 20);
console.log(10 == 10 && 20 != 20);
console.log(10 == 10 || 20 != 20);
console.log(!10 == 10);

/*
Equality without type coersion and 3 equlas sign
*/
console.log("Equality with type coersion and 3 equlas sign");
console.log(typeof 0 + " " + typeof false);
console.log(0 == false);
console.log(typeof "0" + " " + typeof false);
console.log("0" == false);
console.log(typeof 1 + " " + typeof "1");
console.log(0 == 1);
console.log("Equality without type coersion and 3 equlas sign");
console.log(typeof 0 + " " + typeof false);
console.log(0 === false);
console.log(typeof "0" + " " + typeof false);
console.log("0" === false);
console.log(typeof 1 + " " + typeof "1");
console.log(0 === 1);

/*
MAP | FILTER | REDUCE
*/
console.log("MAP | FILTER | REDUCE");
var array = [1, 2, 3, 4, 5];

// MAP: is use to transform
var trans = array.map(function(n){
  return n*2;
});
console.log("1 - MAP: transformation");
console.log(JSON.stringify(trans));

console.log("2 - FILTER: allows you to filter elements on your array");
var filt =  array.filter(function(n){
  return n % 2 == 0
});
console.log(JSON.stringify(filt));
console.log("3 - REDUCE: allows you to reduce all element of array in one element");
var red =  array.reduce(function(accumulator, current){
  return accumulator + current;
});
console.log(JSON.stringify(red));
console.log("CALLBACKS: Is a function that is executed inside another function");
function callbackExample(name, callback) {
  console.log(callback(name));
}
callbackExample("Dongmo Michael", function(name){
  return "Hello " + name;
});
/*console.log("Named Export/Import");
console.log(add(2,2));
console.log(divide(2,2));
console.log(substract(2,2));
console.log(multiplie(2,2));
console.log(PI);*/

//Let keyword

console.log('Let keyword');
for(let i = 0; i<= 10; i++){
  console.log(i);
}

//Const keyword: riassignment is not possible

console.log('Const keyword: riassignment is not possible');
const username =  'Michael';
const userage =  18;
const usercountry =  'Italy';
//username =  'Anna Smith'; // this will not walk

const user = {};
user['name'] = 'Michael';
console.log(user.name);

//Template Literals
console.log('Template Literals');
console.log(`Name ${username} Country is ${usercountry} and user age is ${userage}`);

// Arrays and Spread Operators
console.log('Arrays and Spread Operators');
const vector1 = ['Michael', 'Josee', 'Mederic', 'Ntehlewouh'];
const vector2 = ['Maria', 'Said', 'Ismail', 'Anna'];

console.log('Create new array with spread operator');
const vector3 = [...vector1, ...vector2];
console.log('vector3 = ' + vector3);

const usernameToArray = [...username];
usernameToArray.forEach(function(letter) {
  console.log(letter);
});

console.log('Use spread operator to do arithmetic operation');
const addNumber = function(n1, n2, n3) {
  return n1 +n2 + n3;
}

const numbers = [5, 10, 15];

// logic

const additions = addNumber(...numbers);
console.log(additions);

// Objects and Spread Operators
console.log('Objects and Spread Operators');
const addressObject = {
  city: 'Dschang',
  country: 'Cameroon',
  postCode: 'DSC237'
};

const nameObject = {
  firstName: 'Dongmo',
  lastName: 'Momo'
};

const personObject = {
  ...addressObject,
  ...nameObject
};

console.log('Person data = ' + JSON.stringify(personObject, null, 2));

// Arrow Functions: the way we use arrow function is to replce function keyword with () => symbol

console.log('Arrow Functions');

const hello = function() {
  const es6 = 'ES6';
  return `Hello ${es6}`;
};

const powers = [1, 2, 3, 4, 5].map(function(number, index){
  return Math.pow(number, index);
});

const add = function(n1, n2) {
  return n1 + n2;
};

const milesToKm = function (miles) {
  return miles * 1.60934;
};

console.log(hello());
console.log(powers);
console.log(add(12, 12));
console.log(milesToKm(100));

// doing the exact same thing using arrow function

const hello1 = () => {
  const es6 = 'ES6';
  return `Hello ${es6}`;
};

const powers1 = [12, 5, 9, 4, 8].map((number, index) => Math.pow(number, index));

const add1 = (n1, n2) => n1 + n2;

const milesToKm1 = miles => miles * 1.60934;

console.log('doing the exact same thing using arrow function');

console.log(hello1());
console.log(powers1);
console.log(add1(110, 120));
console.log(milesToKm1(135));

// Lexical this
console.log('Lexical this');
const michael = {
  name: 'Michael',
  cars: ['ValsWaggen', 'Ferrari', 'Lambo'],
  toString: function() {
    //console.log(`${this.name} has ${this.cars}`)
    this.cars.forEach(car => {
      console.log(`${this.name} has ${car}`);
    })
  }
};

michael.toString();

// Enhanced Object Properties
console.log(' Enhanced Object Properties');

const pricePropName = 'PRICE';

const calculator = (name, price) => {
  return {
    name, // this the same as name: name
    add(n1, n2) { // this equal to --> add: function(n1, n2){}
      return n1 + n2;
    },
    [pricePropName.toLowerCase()]: price
  }
};

const calc = calculator('Casio', '€19,99');

console.log(calc.name);
console.log(calc.add(2,4));
console.log(calc.price);